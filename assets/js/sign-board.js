/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 7/01/18 21:52.
 */

"use strict";

//region Global Variables
window.datesArray = [];
window.bookingPrice = [];
window.total = 0; //total of all products (before discount and gst)
window.bookingTotal = 0; //final total that send for merchant

window.discountArray = [];
window.discountDays = 0;
window.discountRate = 0;
window.discountId = null;
window.discountAmount = 0;
//endregion

//region Default settings
$('.booking-form').hide();
$('#image-upload-progress').hide();

getDiscountData();

//endregion

//region Events

//endregion

//region Functions
// [ID: 341]
function getDiscountData() {
    var boardId = $('#boardid').val();

    jQuery.ajax({
        type: 'POST',
        url: window.server + 'signboard/getdiscountdata',
        data: {orgid:7, boardid:boardId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);

                for(var i = 0; res.length > i; i++) {
                    var key = parseInt(res[i].days);
                    window.discountArray[key] = res[i].discount;
                }

                console.log(window.discountArray);
            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
        }
    });
}


// [ID: 228]
function calendarPickDate(id, year, month, date) {
    //develop the function here
    month = addLeadingZeros(month, 2);
    date = addLeadingZeros(date, 2);
    var fullDate = year+'/'+month+'/'+date;
    var rateValue = parseInt(($('#board-cell-rate-'+id+year+month+date).text()).replace(/[^0-9\.]/g, ''));

    if(!isInArray(fullDate, window.datesArray)) {
        $('#board-calendar-'+id+year+month+date).attr('style', 'background-color:#dff3ff');

        window.datesArray.push(fullDate);
        window.bookingPrice.push(rateValue);
        window.total = parseFloat(rateValue+window.total);

        $('.single-enable-disable').fadeIn(200);

        //setup text and numbers

        var appendHtml = '<tr id="booked-row-'+id+year+month+date+'">' +
            '               <td>'+date+ ' ' + idateGetMonthText(month)+'</td>' +
            '               <td align="right">$'+rateValue.toFixed(2)+'</td>' +
            '           </tr>';
        $('#booked-rows').append(appendHtml);
        $('.booking-form').fadeIn(200);

        //Discount start
        if(window.discountArray.length > 0) {
            var selectedDatesNumber = window.datesArray.length;

            for(var days in window.discountArray) {
                if(selectedDatesNumber >= days && discountId == null) {
                    window.discountDays = days
                    window.discountRate = window.discountArray[days];
                }
                else if(selectedDatesNumber >= days && window.discountDays < days) {
                    window.discountDays = days;
                    window.discountRate = window.discountArray[days];
                }
            }

            if(window.discountRate != 0) {
                window.discountAmount = parseFloat(window.total*(window.discountRate/100));

                $('#discount-row').remove();
                var appendDiscount = '  <tr id="discount-row" class="gst-row">' +
                    '                       <td style="padding-top: 15px;">Discount ('+window.discountRate+'%)</td>' +
                    '                       <td style="padding-top: 15px;" align="right">- $'+window.discountAmount.toFixed(2)+'</td>' +
                    '                   </tr>';

                $('#table-footer').prepend(appendDiscount);
            }

            else {
                window.discountAmount = 0;
            }
        }
        //Discount end

        getTotals();
    }
    else {
        $('#board-calendar-'+id+year+month+date).removeAttr('style');
        window.datesArray.splice( $.inArray(fullDate, window.datesArray), 1 );
        window.bookingPrice.splice( $.inArray(rateValue, window.bookingPrice), 1 );
        window.total = parseInt(window.total-rateValue);



        if(window.discountArray.length > 0) {
            selectedDatesNumber = window.datesArray.length;
            var discountApply = 0; //check is there any discounts for the current situation

            for(var days in window.discountArray) {
                if(selectedDatesNumber >= days && discountId == null) {
                    window.discountDays = days
                    window.discountRate = window.discountArray[days];
                    discountApply = 1;
                }
                else if(selectedDatesNumber >= days && window.discountDays < days) {
                    window.discountDays = days;
                    window.discountRate = window.discountArray[days];
                    discountApply = 1;
                }
            }

            if(discountApply == 0) {
                window.discountDays = 0;
                window.discountRate = 0;
                $('#discount-row').remove();
            }
            else {
                window.discountAmount = parseFloat(window.total*(window.discountRate/100));

                $('#discount-row').remove();
                var appendDiscount = '  <tr id="discount-row" class="gst-row">' +
                    '                       <td style="padding-top: 15px;">Discount ('+window.discountRate+'%)</td>' +
                    '                       <td style="padding-top: 15px;" align="right">- $'+window.discountAmount.toFixed(2)+'</td>' +
                    '                   </tr>';

                $('#table-footer').prepend(appendDiscount);
            }
        }



        getTotals();


        if(window.datesArray.length == 0) {
            $('.booking-form').fadeOut(200);
            $('#booked-rows').html('');
        }
        else {
            $('#booked-row-'+id+year+month+date).fadeOut(200, function () {
                $(this).remove();
            })
        }

    }
}

function getTotals() {
    var subTotal = parseFloat(window.total-window.discountAmount);
    console.log('total: '+window.total+' ## Discount: '+window.discountAmount);
    var finalTotal = (parseFloat(subTotal*1.15)).toFixed(2);

    $('#booking-gst').text('$'+(parseFloat(subTotal*0.15)).toFixed(2));
    $('#booking-total').text('$'+finalTotal);
    window.bookingTotal = finalTotal;
}

$(function(){
    // function from the jquery form plugin
    $('.bill-board-image-upload').ajaxForm({
        beforeSend:function(){
            $('.upload-img-overlay').trigger('blur');
            $("#image-upload-progress").show();
            $('#booking-gst').trigger('touchstart');
        },
        uploadProgress:function(event,position,total,percentComplete){
            $("#image-upload-progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
            $("#image-upload-sr-only").html(percentComplete+'%'); // show the percentage number
        },
        success:function(){
            $("#image-upload-progress").hide();
        },
        complete:function(response){
            var res = JSON.parse(response.responseText);

            if(res.imgid == -1) {
                $('#image-error-display').html('Sorry! We accept only <strong>JPG/PNG </strong> images. This image is not in <strong>JPG/PNG format</strong>.');
            }
            else {
                $('#image-error-display').html('');
                $('#img-id').val(res.imgid);
                $('#bill-board-upload-img').attr('src', window.server+'assets/backend/images/signboard/bookings/temp/'+res.imgid+'.'+res.ext+'?timestamp=' + new Date().getTime());
            }

        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
    $("#image-upload-progress-bar").width('0%');
});

// function setHeader(xhr) {
//     var token = '';
//     xhr.setRequestHeader('Authorization', token);
// }

function submitImageUploadForm(form) {
    $(form).submit();
}

function payNow() {

    var name = $.trim($('#name').val());
    var email = $.trim($('#email').val());
    var imgId = $('#img-id').val();
    var orgId = $('#orgid').val();
    var telephone = $('#telephone').val();
    var boardId = $('#boardid').val();
    var error = 0;

    $('.input-error').removeClass('input-error');

    if(name == '') {
        error = 1;
        $('.name').addClass('input-error');
    }
    if(email == '') {
        error = 1;
        $('.email').addClass('input-error');
    }
    if(imgId == '') {
        error = 1;
        $('.image').addClass('input-error');
    }

    if(error == 0) {

        $('#polipay-button-container').html('<p style="text-align: center"><img src="'+window.domain+'assets/img/loading/loader.gif" /> </p>');

        jQuery.ajax({
            type: 'POST',
            url: window.server + 'signboard/savesignbooking',
            data: {bookingtotal:window.bookingTotal, orgid:orgId, boardid:boardId, name:name, email:email, imgid:imgId, dates:window.datesArray, singleamount:window.bookingPrice, telephone:telephone, discount:window.discountRate, type:3}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    var res = JSON.parse(data);

                    if(res == -1) {
                        //image not in the range of accepted width and height
                        $('#image-error-display').html('Sorry! Your image is not greater than minimum width or/and height. Please try again with a higher resolution image.');
                        $('#polipay-button-container').html('<img class="pull-right" style="cursor: pointer;" onclick="payNow();" src="'+window.server+'assets/backend/images/merchants/pay-buttons/poli.png" />');
                    }
                    else {
                        $('#image-error-display').html('');
                        window.location.replace(res.NavigateURL);
                    }

                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
            }
        });
    }
    else {
        var myDiv = document.getElementById('telephone');
        scrollTo(myDiv, 0, 100);
    }
}

function isInArray(value, array) {
    return array.indexOf(value) > -1;
}
//endregion