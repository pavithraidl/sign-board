/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 5/01/18 11:48.
 */

"use strict";

window.server = 'https://crm.prisu.nz/';
window.domain = 'http://isuru.host/staging/sign-board/';