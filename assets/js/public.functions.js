/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 7/01/17 09:40.
 */

"use strict";

//region Global Variables

//endregion

$(document).ready(function () {
    setInterval(function () {
        onlineState();
    }, 5000);
});

//region Functions
//Loading
function blockUI(object) {
    if(object.find('.widget-loading').length != 1) {
        object.addClass('widget-loading');
        object.fadeTo(300, 0.5).find("*").attr("disabled", "disabled");
        var loadingHtml = '<object id="widget-loading-icon" data='+window.domain+'assets/backend/images/loading/ring-sm.svg style="z-index: 100; position: absolute; top: 48%; left: 48%;" type="image/svg+xml"></object>';
        object.parent().append(loadingHtml);
    }
}

function unBlockUI(object) {
    $('#widget-loading-icon').fadeOut(200);
    setTimeout(function () {
        $('#widget-loading-icon').remove();
    });
    object.removeClass('widget-loading');
    object.fadeTo(200, 1).find("*").removeAttr("disabled");
}


//Notification
function notifyDone(msg) {
    new PNotify({
        title: 'Done',
        text: msg,
        type: 'success',
        styling: 'bootstrap3'
    });
}

function dbError() {
    new PNotify({
        title: 'Error Occurred!',
        text: 'Error with the server. Try again later...',
        type: 'error',
        hide: false,
        styling: 'bootstrap3'
    });
}

function noConnection(errorThrown) {
    if(errorThrown == 'Unauthorized') {
        location.reload();
    }
    else if(errorThrown == '') {
        new PNotify({
            title: 'No Connection!',
            text: 'You are not connected with internet. Please check your internet connection.',
            type: 'warning',
            hide: false,
            styling: 'bootstrap3'
        });
    }
    else {
        new PNotify({
            title: 'Oops!',
            text: 'Some Error Occurred. Try again later.',
            type: 'warning',
            hide: false,
            styling: 'bootstrap3'
        });
    }
}

function nconfirm(id, title, question, button, type, top, left) {
    if($('#confirm-'+id).length) {
        setTimeout(function () {
            $('#confirm-'+id).toggleClass('active');
        }, 40);

    }
    else {
        var style = top != null ? 'top: '+top+'% !important;': '';
        style += left != null ? ' left: '+left+'% !important;': '';

        var color = 'default',
            icon = 'fa-exclamation-circle';

        if(type == 'danger') {
            color = 'red';
            icon = 'fa-times-circle';
        }
        else if(type == 'success') {
            color = 'green';
            icon = 'fa-check-circle-o';
        }
        else if(type == 'info') {
            color = 'blue';
            icon = 'fa-bullhorn';
        }
        else if(type == 'warning') {
            color = 'orange';
            icon = 'fa-bolt';
        }

        var htmlAppend = '  <div id="confirm-'+id+'" class="popup-confirm" draggable="true" style="z-index: 1000 !important; '+style+'"> ' +
            '                       <div id="confirm-content-'+id+'" class="content"> ' +
            '                           <h2><i class="fa '+icon+' '+color+'" style="font-size: 25px;"></i> '+title+'</h2> ' +
            '                           <p>'+question+'</p> ' +
            '                           <p style="text-align: center;"><button class="btn btn-sm btn-default nconfirm-btn-cancle" onclick="cancelNConfirm(\''+id+'\')">Cancel</button><button class="btn btn-sm btn-'+type+'" onclick="window.confirmfunction();">'+button+'</button> </p> ' +
            '                       </div> ' +
            '                   </div>';

        $('#'+id).parent().append(htmlAppend);
        setTimeout(function () {
            $('#confirm-'+id).toggleClass('active');
        }, 40);
    }
}

function cancelNConfirm(id) {
    window.notConfirmed();
    $('#confirm-content-'+id).fadeOut(200);
    $('#confirm-'+id).removeClass('active');
    setTimeout(function () {
        $('#confirm-content-'+id).fadeIn(100);
    },600);
}

$(function(){
    //listen for click events from this style
    $(document).on('click', '.notifyjs-metro-base .no', function() {
        //programmatically trigger propogating hide event
        window.notConfirmed();
        $(this).trigger('notify-hide');
    });
    $(document).on('click', '.notifyjs-metro-base .yes', function() {
        //show button text
        window.confirmfunction();
        //hide notification
        $(this).trigger('notify-hide');
    });
});



//search filter
function searchFilter(object){
    var filter = $("#"+object.id).val(), count = 0;
    var elements = 0;
    $(".search-filter li").each(function(){
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut(200);
        } else {
            $(this).show();
            count++;
        }
        if($(this).is(':visible')) {
            elements++;
        }
    });

    if(elements == 0) {
        window.searchListEmpty();
    }
    var numberItems = count;
    $("#filter-count").text("Number of Comments = "+count);
}

function tableSearchFilter(object){
    var filter = $("#"+object.id).val(), count = 0;
    var elements = 0;
    $(".search-filter tr").each(function(){
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut(200);
        } else {
            $(this).show();
            count++;
        }
        if($(this).is(':visible')) {
            elements++;
        }
    });

    if(elements == 0) {
        window.searchListEmpty();
    }
    var numberItems = count;
    $("#filter-count").text("Number of Comments = "+count);
}
//endregion

//set divpopup
function popupDiv(appendId, id, title, subTitle, styles) {

    var htmlAppend = '  <div id="popup-div-'+id+'" class="popup-div" draggable="true" style="z-index: 999 !important; '+styles+'">' +
        '                       <div class="col-md-12 col-sm-12 col-xs-12"> ' +
        '                           <div id="popup-div-xpanel-'+id+'" class="x_panel"> ' +
        '                               <div class="x_title"> ' +
        '                                   <h2 id="div-popup-title-'+id+'">'+title+' <small id="div-popup-sub-title-'+id+'">'+subTitle+'</small></h2> ' +
        '                                   <ul class="nav navbar-right panel_toolbox"> ' +
        '                                       <li id="div-pupup-close" onclick="closePopupDiv(\''+id+'\');"><a class="close-link"><i class="fa fa-close"></i></a></li> ' +
        '                                   </ul> ' +
        '                                   <div class="clearfix"></div> ' +
        '                               </div> ' +
        '                               <div id="div-popup-content-'+id+'" class="x_content" style="max-height: 435px;"><br/><br/><br/></div> ' +
        '                           </div> <button id="btn-close-popup-div" type="button" class="btn btn-default pull-right" onclick="closePopupDiv(\''+id+'\');" data-dismiss="modal">Close</button>' +
        '                       </div> ' +
        '               </div>';

    $('#'+appendId).append(htmlAppend);
    var el = $('#div-popup-content-'+id);
    blockUI(el);

    setTimeout(function () {
        $('#popup-div-xpanel-'+id).fadeIn(100);
        $('#popup-div-'+id).toggleClass('active');
    }, 40);
}

function closePopupDiv(id) {
    $('#btn-close-popup-div').fadeOut(10);
    $('#popup-div-xpanel-'+id).fadeOut(100, function () {
        setTimeout(function() {
            $('#popup-div-'+id).remove();
        }, 500);
    });
    $('#popup-div-'+id).removeClass('active');
}


function updateSingleField(object, table, where, field, activity, id) {
    var objectId = $('#'+object.id);
    var value = objectId.val();

    if($('#'+object.id+'-loading-icon').length == 0) {
        var loadingHtml = '<object id="'+object.id+'-loading-icon" data='+window.domain+'assets/images/loading/pending-sm.svg style="z-index: 100; position: absolute; top: 18%; left: 80%;" type="image/svg+xml"></object>';
        objectId.parent().append(loadingHtml);
    }


    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'public/update-single-field',
        data:{table:table, where:where, field:field, value:value, activity:activity, id:id},
        success: function (data) {
            if(data == 'null') {

            }
            else if(data == 1) {
                $('#'+object.id+'-loading-icon').hide();
                $('#'+object.id+'-loading-icon').remove();
                var loadingHtml = $('<img id="'+object.id+'-loading-icon" src='+window.domain+'assets/images/loading/checked-sm.png style="z-index: 100; position: absolute; top: 18%; left: 80%; height: 30px;" />').hide();
                objectId.parent().append(loadingHtml);
                $('#'+object.id+'-loading-icon').fadeIn(200);
                setTimeout(function () {
                    $('#'+object.id+'-loading-icon').fadeOut(1000);
                }, 1500);

            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function updateSingleData(value, table, where, field, activity, id) {

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'public/update-single-field',
        data:{table:table, where:where, field:field, value:value, activity:activity, id:id},
        success: function (data) {
            if(data == 'null') {

            }
            else if(data == 1) {
                return 1;
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function addLeadingZeros (n, length)
{
    var str = (n > 0 ? n : -n) + "";
    var zeros = "";
    for (var i = length - str.length; i > 0; i--)
        zeros += "0";
    zeros += str;
    return n >= 0 ? zeros : "-" + zeros;
}

function onlineState() {
    // jQuery.ajax({
    //     type: 'POST',
    //     url: window.server + 'users/onlineState',
    //     data: {},
    //     success: function (data) {
    //         if(data == 'Unauthorized') {
    //             window.location.reload();
    //         }
    //         else if(data != 0) {
    //
    //         }
    //         else {
    //             console.log('online state error!');
    //         }
    //     },
    //     error: function (xhr, textStatus, errorThrown) {
    //         console.log('online state no connection');
    //     }
    // });
}

function idateGetMonthText(MonthNumber) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if(MonthNumber.length < 2) {
        MonthNumber = MonthNumber.replace('0', '');
    }
    return months[--MonthNumber];
}

(function($) {
    $.fn.animateNumbers = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var isInput = $this.is('input');
            var start = parseInt(isInput ? $this.val().replace(/,/g, "") : $this.text().replace(/,/g, ""));
            var regex = /(\d)(?=(\d\d\d)+(?!\d))/g;
            commas = commas === undefined ? true : commas;

            // number inputs can't have commas or it blanks out
            if (isInput && $this[0].type === 'number') {
                commas = false;
            }

            $({value: start}).animate({value: stop}, {
                duration: duration === undefined ? 1000 : duration,
                easing: ease === undefined ? "swing" : ease,
                step: function() {
                    isInput ? $this.val(Math.floor(this.value).toFixed(2)) : $this.text(Math.floor(this.value).toFixed(2));
                    if (commas) {
                        isInput ? $this.val($this.val().replace(regex, "$1,")) : $this.text($this.text().replace(regex, "$1,"));
                    }
                },
                complete: function() {
                    if (parseInt($this.text()) !== stop || parseInt($this.val()) !== stop) {
                        isInput ? $this.val(stop) : $this.text(stop);
                        if (commas) {
                            isInput ? $this.val($this.val().replace(regex, "$1,")) : $this.text($this.text().replace(regex, "$1,"));
                        }
                    }
                }
            });
        });
    };
})(jQuery);


(function( $ ) {
    $.fn.playLivIcon = function(Duration, RepeatTimes ) {
        this.playLiviconEvo({duration: 1, repeat: 6}, true);
    };
}( jQuery ));