/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 5/01/18 11:45.
 */

"use strict";

//region Global Variables
getBillBoards();

//endregion

//region Default settings

//endregion

//region Events

//endregion

//region Functions
function getBillBoards() {
    //develop the function here
    var orgId = $('#orgid').val();
    //ajax method
    jQuery.ajax({
        type: 'GET',
        url: window.server + 'signboard/getboards',
        data: {orgid:orgId}, //add the parameter if required to pass
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0) {
                dbError();
            }
            else {
                var res = JSON.parse(data);
                var appendHtml = '';

                for(var i = 0; i < res.length; i++) {
                    if(res[i].img == 1) var imgName = res[i].id+'.jpg';
                    else var imgName = 'default.jpg';

                    var overLay = res[i].status == 2 ? '<div class="coming-soon"><h4>Temporary Disable!</h4></div>' : res[i].status == 3 ? '<div class="coming-soon"><h4>Coming Soon!</h4></div>' : '';

                    var append = '                   <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0.2s" style="position: relative;">'+ overLay +
                        '                       <div class="panel text-center single-blog">' +
                        '                           <img src="'+window.server+'assets/backend/images/signboard/boards/'+imgName+'" class="img-full" alt="">' +
                        '                           <div class="padding-20">' +
                        '                               <div class="space-10"></div>' +
                        '                               <a href="'+window.domain+'sign-board/'+res[i].id+'"><h3>'+res[i].location+'</h3></a>' +
                        '                               <div class="space-15"></div>' +
                        '                               <p>'+res[i].smalldescription+'</p>' +
                        '                               <div class="space-20"></div>' +
                        '                               <a href="'+window.domain+'sign-board/'+res[i].id+'" class="btn btn-link">Select Location</a>' +
                        '                            <div class="space-20"></div>' +
                        '                           </div>' +
                        '                       </div>' +
                        '                   </div>';

                    if(res[i].status == 1) {
                        appendHtml += '<a href="'+window.domain+'sign-board/'+res[i].id+'">'+append+'</a>';
                    }
                    else {
                        appendHtml += append;
                    }
                }

                $('#bill-board-container').append(appendHtml);

            }
        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unBlockUI(el);
        }
    });
}

function submitMessage() {
    var name = $.trim($('#form-name').val());
    var email = $.trim($('#form-email').val());
    var subject = $.trim($('#form-subject').val());
    var msg = $.trim($('#form-message').val());
    var orgId = $('#orgid').val();
    var key = $('#key').val();
    var secret = $('#secret').val();

    if(name == '' || email == '' || subject == '' || msg == '') {

    }
    else {
        //ajax method
        jQuery.ajax({
            type: 'POST',
            url: window.server + 'website/sendemail',
            data: {name:name, email:email, phone:'', subject:subject, message:msg, orgkey:key , orgsecret:secret , orgid: orgId}, //add the parameter if required to pass
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                else {
                    console.log('done');
                }
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unBlockUI(el);
            }
        });
    }
}
//endregion