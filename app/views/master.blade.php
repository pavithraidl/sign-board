<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 31/12/17 10:56.
 */
?>

        <!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Your Bill Board</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{URL::To('/')}}/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="{{URL::To('/')}}/assets/images/icon.png" />

    <!-- Plugin-CSS -->
    {{HTML::style('/assets/css/themify-icons.css')}}
    {{HTML::style('/assets/css/bootstrap.min.css')}}
    {{HTML::style('/assets/css/owl.carousel.min.css')}}

    {{HTML::style('/assets/css/animate.css')}}
    {{HTML::style('/assets/css/magnific-popup.css')}}

    <!-- Main-Stylesheets -->
    {{HTML::style('/assets/css/space.css')}}
    {{HTML::style('/assets/css/theme.css')}}
    {{HTML::style('/assets/css/overright.css')}}
    {{HTML::style('/assets/css/normalize.css')}}
    {{HTML::style('/assets/css/style.css?v=0.01')}}
    {{HTML::style('/assets/css/responsive.css')}}
    {{HTML::script('assets/js/vendor/modernizr-2.8.3.min.js')}}
</head>
<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="preloade">
<span><i class="ti-blackboard"></i></span>
</div>

<!--Header-Area-->
<header class="relative fix" id="home">
<div class="section-bg overlay-bg dewo bg-slide">
    <div class="item">
        <img src="{{URL::To('/')}}/assets/images/slide/slide1.jpg" alt="">
    </div>
    <div class="item">
        <img src="{{URL::To('/')}}/assets/images/slide/slide2.jpg" alt="">
    </div>
    <div class="item">
        <img src="{{URL::To('/')}}/assets/images/slide/slide3.jpg" alt="">
    </div>
    <div class="item">
        <img src="{{URL::To('/')}}/assets/images/slide/slide4.jpg" alt="">
    </div>
</div>
<!--Mainmenu-->
<nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix" data-offset-top="60">
    <div class="container">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ URL::To('/') }}" class="navbar-brand">
                <?php
                if($contentObject[45][75]['logo'] == 1) {
                    $logoUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$contentObject[45][75]['logo_id'].'.'.$contentObject[45][75]['logo_ext'];
                }
                else {
                    $logoUrl = 'plese set to default';
                }
                ?>
                <img src="{{ $logoUrl }}" style="margin-top: -10px;" />
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="mainmenu">
            <ul class="nav navbar-nav">
                <li><a href="{{URL::To('/')}}#home">Home</a></li>
                <li><a href="{{URL::To('/')}}#bill-boards">Bill Boards</a></li>
                <li><a href="{{URL::To('/')}}#how-it-works">How It Works</a></li>
                <li><a href="{{URL::To('/')}}#features">Features</a> </li>
                <li><a href="{{URL::To('/')}}#faq">FAQ</a> </li>
                <li><a href="{{URL::To('/')}}#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
<!--Mainmenu/-->
<div class="space-100"></div>
<div class="space-20 hidden-xs"></div>



    @yield('content')


    <div class="space-80"></div>
    <div class="row text-white wow fadeIn">
        <div class="col-xs-12 text-center">
            <div class="space-20"></div>
            <p>Yourbillboard Limited {{ date('Y') }} All rights reserved.</p>
        </div>
    </div>
    <div class="space-20"></div>
</div>
</footer>
<!--Footer-area-->
{{HTML::script('assets/js/constant.js?v=0.01')}}
<!--Vendor JS-->
{{HTML::script('assets/js/vendor/jquery-1.12.4.min.js')}}
{{HTML::script('assets/js/vendor/bootstrap.min.js')}}
<!--Plugin JS-->
{{HTML::script('assets/js/owl.carousel.min.js')}}
{{HTML::script('assets/js/scrollUp.min.js')}}
{{HTML::script('assets/js/magnific-popup.min.js')}}
{{HTML::script('assets/js/ripples-min.js')}}
{{HTML::script('assets/js/contact-form.js')}}
{{HTML::script('assets/js/spectragram.min.js')}}
{{HTML::script('assets/js/ajaxchimp.js')}}
{{HTML::script('assets/js/wow.min.js')}}
{{HTML::script('assets/js/plugins.js')}}
<!--Active JS-->
{{HTML::script('assets/js/main.js')}}
<!--Maps JS-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
{{HTML::script('assets/js/maps.js')}}

@yield('scripts')
</body>
</html>













