<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 4/01/18 15:20.
 */
?>

<?php   $variable = new VariableController();
        $orgId = VariableController::orgId;
        $server = VariableController::server;
        $orgKey = VariableController::key;
        $orgSecret = VariableController::secret;
    ?>

@extends('master')

@section('content')
    <input type="hidden" id="orgid" value="{{ $orgId }}" />
    <input type="hidden" id="key" value="{{ $orgKey }}" />
    <input type="hidden" id="secret" value="{{ $orgSecret }}" />
    <!--Header-Text-->
    <div class="container text-white">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="space-100"></div>
                <h1>{{ $contentObject[39][59]['slider_title'] }}</h1>
                <div class="space-10"></div>
                <p>{{ $contentObject[39][59]['slider_content'] }}</p>
                <div class="space-50"></div>
                {{--<a href="{{ $contentObject[39][59]['video_url'] }}" class="btn btn-icon video-popup"><span class="ti-control-play"></span>Watch Video</a>--}}
            </div>
            <div class="hidden-xs hidden-sm col-md-4 bill-board-slider">
                <div class="home_screen_slide">
                    <div class="single_screen_slide wow fadeInRight">
                        @foreach($contentObject[40] as $slide)
                            <?php
                                if($slide['image'] == 1) {
                                    $imgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$slide['image_id'].'.'.$slide['image_ext'];
                                }
                                else {
                                    $imgUrl = 'plese set to default';
                                }
                            ?>
                            <div class="item"><img src="{{ $imgUrl }}" alt="" style="width: 390px !important; height: 150px !important;"></div>
                        @endforeach
                    </div>
                </div>
                <div class="home_screen_nav">
                    <span class="ti-angle-left testi_prev"></span>
                    <span class="ti-angle-right testi_next"></span>
                </div>
            </div>
        </div>
        <div class="space-80"></div>
    </div>
    <!--Header-Text/-->
    </header>
    <!--Header-Area/-->
    <!--Blog-Section-->
    <section id="bill-boards">
        <div class="space-80"></div>
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3 class="text-uppercase">Bill Boards</h3>
                    <p>Please select your Billboard location.</p>
                </div>
            </div>
            <div class="space-60"></div>
            <div class="row" id="bill-board-container">
                {{--Jquyer append--}}
            </div>
        </div>
    </section>
    <!--Blog-Section/-->
    <!--Work-Section-->
    <section class="gray-bg" id="how-it-works">
        <div class="space-80"></div>
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3 class="text-uppercase">How it works</h3>
                    <p>Follow these easy steps...</p>
                </div>
            </div>
            <div class="space-60"></div>
            <div class="row">
                <?php $counter = 1; ?>
                @foreach($contentObject[41] as $steps)
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center wow fadeInUp" data-wow-delay="0.{{ $counter*2 }}s">
                        <div class="hover-shadow">
                            <div class="space-60">
                                <img src="{{URL::To('/')}}/assets/images/icon/icon{{ $counter }}.png" alt="">
                            </div>
                            <div class="space-20"></div>
                            <h5 class="text-uppercase">{{ $steps['title'] }}</h5>
                            <p>{{ $steps['content'] }}</p>
                        </div>
                    </div>
                    <?php $counter++ ?>
                @endforeach
            </div>
            <div class="space-60" style="height: auto;">
                <h4 style="text-align: center; color: #2a3875;">{{ $contentObject[42][69]['title'] }}</h4>
                <p style="margin-top: 30px; color: #979797; font-style: italic;"><strong>Info: </strong>{{ $contentObject[42][69]['info'] }}</p>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 text-center wow fadeInUp">
                    <div class="down-offset ">
                        <?php
                            if($contentObject[42][69]['image'] == 1) {
                                $imgUrl = 'https://crm.prisu.nz/assets/backend/images/website/slide-block/'.$orgId.'/'.$contentObject[42][69]['image_id'].'.'.$contentObject[42][69]['image_ext'];
                            }
                            else {
                                $imgUrl = 'plese set to default';
                            }
                        ?>
                        <img src="{{ $imgUrl }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Work-Section/-->
    <!--Feature-Section-->
    <section class="fix">
        <div class="space-60"></div>
        <div class="container" id="features">
            <div class="space-100"></div>
            <div class="row wow fadeInUp">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3 class="text-uppercase">Features</h3>
                    <p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incididunt ut labore Lorem ipsum madolor sit amet.</p>
                </div>
            </div>
            <div class="space-60"></div>
            <div class="row feature-area">
                <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInLeft">
                    <div class="space-30"></div>
                    <a href="index5.html#feature1" data-toggle="tab">
                        <div class="media single-feature">
                            <div class="media-body text-right">
                                <h5>SUPER BRIGHTNESS</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididugnt ut labore</p>
                            </div>
                            <div class="media-right">
                                <div class="border-icon">
                                    <span class="ti-shine"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="space-30"></div>
                    <a href="index5.html#feature2" data-toggle="tab">
                        <div class="media single-feature">
                            <div class="media-body text-right">
                                <h5>TRUE TONE COLORS</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididugnt ut labore</p>
                            </div>
                            <div class="media-right">
                                <div class="border-icon">
                                    <span class="ti-brush-alt"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="space-30"></div>
                    <a href="index5.html#feature3" data-toggle="tab">
                        <div class="media single-feature">
                            <div class="media-body text-right">
                                <h5>WIDE SCREEN</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididugnt ut labore</p>
                            </div>
                            <div class="media-right">
                                <div class="border-icon">
                                    <span class="ti-layout-slider"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="space-30"></div>
                </div>
                <div class="hidden-xs hidden-sm col-md-4 text-center fix wow fadeIn">
                    <div class="down-offset relative ">
                        <img src="{{URL::To('/')}}/assets/images/frames/full-board.png" alt="">
                        <div class="screen_image tab-content" style="z-index: -1">
                            <div id="feature1" class="tab-pane fade in active" style="width: 350px; height: 110px;">
                                <img src="{{URL::To('/')}}/assets/images/feature/1.jpg" alt="">
                            </div>
                            <div id="feature2" class="tab-pane fade" style="width: 350px; height: 110px;">
                                <img src="{{URL::To('/')}}/assets/images/feature/3.jpg" alt="">
                            </div>
                            <div id="feature3" class="tab-pane fade" style="width: 350px; height: 110px;">
                                <img src="{{URL::To('/')}}/assets/images/feature/5.jpg" alt="">
                            </div>
                            <div id="feature4" class="tab-pane fade" style="width: 350px; height: 110px;">
                                <img src="{{URL::To('/')}}/assets/images/feature/2.jpg" alt="">
                            </div>
                            <div id="feature5" class="tab-pane fade" style="width: 350px; height: 110px;">
                                <img src="{{URL::To('/')}}/assets/images/feature/4.jpg" alt="">
                            </div>
                            <div id="feature6" class="tab-pane fade" style="width: 350px; height: 110px;">
                                <img src="{{URL::To('/')}}/assets/images/feature/6.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInRight">
                    <div class="space-30"></div>
                    <a href="index5.html#feature4" data-toggle="tab">
                        <div class="media single-feature">
                            <div class="media-left">
                                <div class="border-icon">
                                    <span class="ti-eye"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                <h5>DETAILED IMAGES</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididugnt ut labore</p>
                            </div>
                        </div>
                    </a>
                    <div class="space-30"></div>
                    <a href="index5.html#feature5" data-toggle="tab">
                        <div class="media single-feature">
                            <div class="media-left">
                                <div class="border-icon">
                                    <span class="ti-shine"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                <h5>HIGH RESOLUTION</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididugnt ut labore</p>
                            </div>
                        </div>
                    </a>
                    <div class="space-30"></div>
                    <a href="index5.html#feature6" data-toggle="tab">
                        <div class="media single-feature">
                            <div class="media-left">
                                <div class="border-icon">
                                    <span class="ti-calendar"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                <h5>24 x 7</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididugnt ut labore</p>
                            </div>
                        </div>
                    </a>
                    <div class="space-30"></div>
                </div>
            </div>
        </div>
    </section>
    <hr>
    <!--Question-section-->
    <section id="faq" class="fix">
        <div class="space-80"></div>
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3 class="text-uppercase">Frequently asked questions</h3>
                </div>
            </div>
            <div class="space-60"></div>
            <div class="row">
                <div class="col-xs-12 col-md-6 wow fadeInUp">
                    <div class="space-60"></div>
                    <div class="panel-group" id="accordion">
                        <?php $c = 0; ?>
                        @foreach( $contentObject[43] as $question)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="index5.html#collapse{{ $c }}">{{ $question['question'] }}</a></h4>
                            </div>
                            <div id="collapse{{ $c }}" class="panel-collapse collapse @if($c == 0) in @endif">
                                <div class="panel-body">{{ $question['answer'] }}</div>
                            </div>
                        </div>
                        <?php $c++; ?>
                        @endforeach
                    </div>
                </div>
                <div class="hidden-xs hidden-sm col-md-5 col-md-offset-1 wow fadeInRight ">
                    <img src="{{URL::To('/')}}/assets/images/frames/middle.png" alt="">
                </div>
            </div>
        </div>
        <div class="space-80"></div>
    </section>
    <!--Question-section/-->
    <!--Map-->
    <div id="contact"></div>
    <div>

        <iframe src="{{ $contentObject[44][74]['google_map_url'] }}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!--Map/-->
    <!--Footer-area-->
    <footer class="black-bg">
        <div class="container">
            <div class="row">
                <div class="offset-top">
                    <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="well well-lg">
                            <h3>Get in Touch</h3>
                            <div class="space-20"></div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-name" class="sr-only">Name</label>
                                        <input type="text" class="form-control" id="form-name" placeholder="Name" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="form-email" class="sr-only">Email</label>
                                        <input type="email" class="form-control" id="form-email" placeholder="Email" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="form-subject" class="sr-only">Subject</label>
                                        <input type="text" class="form-control" id="form-subject" placeholder="Subject" required>
                                    </div>
                                    <div class="space-10"></div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="form-message" class="sr-only">Message</label>
                                        <textarea class="form-control" rows="6" id="form-message" name="form-message" placeholder="Message" required></textarea>
                                    </div>
                                    <div class="space-10"></div>
                                    <button class="btn btn-link no-round text-uppercase" type="submit" onclick="submitMessage();">Send message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                        <div class="well well-lg">
                            <h3>Address</h3>
                            <div class="space-20"></div>
                            <p>{{ str_replace(',', ',<br/>', $contentObject[44][74]['address']) }}</p>
                            <div class="space-25"></div><br/>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="border-icon sm"><span class="ti-headphone"></span></div>
                                    </td>
                                    <td><a href="callto:{{ $contentObject[44][74]['telephone_number'] }}">{{ $contentObject[44][74]['telephone_number'] }}</a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="border-icon sm"><span class="ti-email"></span></div>
                                    </td>
                                    <td>
                                        <a href="mailto:marveltheme@gmail.com">{{ $contentObject[44][74]['email'] }}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="border-icon sm"><span class="ti-location-pin"></span></div>
                                    </td>
                                    <td>
                                        <address>{{ $contentObject[44][74]['address'] }}</address>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
@endsection


@section('scripts')
    {{HTML::script('assets/js/home.js?v=0.02')}}
@endsection