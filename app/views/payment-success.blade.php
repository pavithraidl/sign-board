<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 19/02/18 15:50.
 */
?>

<?php   $variable = new VariableController();
$orgId = $variable->getVariable('orgid');
$server = $variable->getVariable('server');
?>

        <!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Your Bill Board</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{URL::To('/')}}/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="{{URL::To('/')}}/assets/images/icon.png" />

    <!-- Plugin-CSS -->
{{HTML::style('/assets/css/themify-icons.css')}}
{{HTML::style('/assets/css/bootstrap.min.css')}}
{{HTML::style('/assets/css/owl.carousel.min.css')}}

{{HTML::style('/assets/css/animate.css')}}
{{HTML::style('/assets/css/magnific-popup.css')}}

<!-- Main-Stylesheets -->
    {{HTML::style('/assets/css/space.css')}}
    {{HTML::style('/assets/css/theme.css')}}
    {{HTML::style('/assets/css/overright.css')}}
    {{HTML::style('/assets/css/normalize.css')}}
    {{HTML::style('/assets/css/style.css?v=0.01')}}
    {{HTML::style('/assets/css/responsive.css')}}
    {{HTML::script('assets/js/vendor/modernizr-2.8.3.min.js')}}

    {{--Image Uploader--}}
    <script src="https://crm.prisu.nz/assets/backend/libs/jquery/jquery-1.11.1.min.js"></script>
    <script src="https://crm.prisu.nz/assets/backend/libs/jquery-forms/jquery.form.js"></script>
</head>
<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="preloade">
    <span><i class="ti-blackboard"></i></span>
</div>

<!--Header-Area-->
<header class="relative fix" id="home">
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top affix" data-offset-top="60">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index5.html#" class="navbar-brand">
                <!--<img src="{{URL::To('/')}}/assets/images/logo.png" alt="">-->
                    <img src="{{URL::To('/')}}/assets/images/logo.png" style="width: 250px; height: auto; margin-top: -10px;" />
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">
                    <li><a href="{{URL::To('/')}}#home">Home</a></li>
                    <li><a href="{{URL::To('/')}}#bill-boards">Bill Boards</a></li>
                    <li><a href="{{URL::To('/')}}#how-it-works">How It Works</a></li>
                    <li><a href="{{URL::To('/')}}#features">Special Features</a> </li>
                    <li><a href="{{URL::To('/')}}#faq">FAQ</a> </li>
                    <li><a href="{{URL::To('/')}}#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="space-100"></div>
    <div class="space-20 hidden-xs"></div>
</header>
{{--Content start--}}



<!--Blog-Section-->
    <section>
        <div class="space-80"></div>
        <div class="container">
            @if($status == 1)
                <div class="row">
                    <h3 style="text-align: center; margin-top:-85px;">Thank You for your order</h3>
                    <div class="col-md-10 col-md-offset-1 invoice">
                        <div class="row">
                            <div class="col-md-12 header">

                                <div class="col-md-6">
                                    <h2>INVOICE</h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <a href="tel:09 363 2001">Ph: 09 363 2001</a>
                                        <a href="mailto:tdprturei@gmail.com">tdprturei@gmail.com</a>
                                        <a href="yourbillboard.co.nz">yourbillboard.co.nz</a><br/>
                                        <span style="color: #fff;">YOURBILLBOARD LIMITED<br/>GST: 125-376-010</span>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#">
                                            111 Newton Road <br/>
                                            Eden Terrace<br/>
                                            Auckland<br/>
                                            1010<br/>
                                            New Zealand
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row info">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <p>
                                        Billed To:<br/>
                                        <strong>{{ $booking[0]['name'] }}</strong><br/>
                                        <strong>{{ $booking[0]['email'] }}</strong><br/>
                                        <strong>{{ $booking[0]['telephone'] }}</strong>
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <p>
                                        Invoice Number:<br/>
                                        <strong>{{ str_pad($id, 5, '0', STR_PAD_LEFT) }}</strong><br/><br/>
                                        Date Of issue:<br/>
                                        <strong>{{ date('d/m/Y') }}</strong>
                                    </p>
                                </div>
                                <div class="col-md-5 big-total">
                                    <p>Invoice Total</p>
                                    <h4 style="color: #000;">${{ number_format($booking[0]['amount'], 2, '.', ',') }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 border">
                                {{--Border--}}
                            </div>
                            <div class="col-xs-12">
                                <table class="table table-responsive">
                                    <thead>
                                    <tr>
                                        <th>
                                            Bill Image
                                        </th>
                                        <th>
                                            Display Date
                                        </th>
                                        <th align="right" style="text-align: right;">
                                            Amount
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $total = 0;
                                        ?>
                                        @foreach($booking as $row)
                                        <tr>
                                            <?php
                                                $originalDate = $row['date'];
                                                $formattedDate = date('D d F Y', strtotime($originalDate));
                                                $total += floatval($row['singleamount']);
                                            ?>
                                            <td><img src="{{ $server }}assets/backend/images/signboard/bookings/{{ $row['boardid'] }}-{{ preg_replace("/[^0-9]/","",$row['date']) }}/{{ $row['imgid'] }}.{{ $row['img_ext'] }}"/> </td>
                                            <td>{{ $formattedDate }}</td>
                                            <td align="right">${{ number_format($row['singleamount'], 2, '.', ',') }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2" align="right" style="padding: 2px; padding-top: 40px !important;">Subtotal</td>
                                            <td style="padding: 2px; padding-top: 40px !important;" align="right"><strong>${{ number_format($total, 2, '.', ',') }}</strong></td>
                                        </tr>
                                        @if($booking[0]['discount'] > 0)
                                            <?php
                                                $discountAmount = floatval($total*($booking[0]['discount']/100));
                                                $total = $total-$discountAmount;
                                            ?>
                                            <tr>
                                                <td style="padding: 2px !important;" colspan="2" align="right">Discount ({{ $booking[0]['discount'] }}%)</td>
                                                <td style="padding: 2px !important;" align="right"><strong>- ${{ number_format($discountAmount, 2, '.', ',') }}</strong></td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td style="padding: 2px !important;" colspan="2" align="right">GST</td>
                                            <?php $tax = floatval($total)*0.15; ?>
                                            <td style="padding: 2px !important;" align="right"><strong>${{ number_format($tax, 2, '.', ',') }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px !important;" colspan="2" align="right">Total</td>
                                            <td style="padding: 2px !important; color: #63acad; font-size: 15px;" align="right"><strong style="color: #505050;">${{ number_format(floatval($total)+floatval($tax), 2, '.', ',') }}</strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="footer-text" style="font-size: 14px;">
                                    A copy of this invoice will be sent to your email for your reference.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <h3 style="text-align: center; margin-top:-85px;">Sorry!</h3>
                    <p style="text-align: center"> Your order is not successful. Please try again or contact <a href="tel:0800002759">0800 002 759</a> </p>
                </div>
            @endif
        </div>
    </section>
    <!--Blog-Section/-->

    <hr>
    {{--content end--}}
    <div class="space-80"></div>
    <div class="row text-white wow fadeIn ifooter">
        <div class="col-xs-12 text-center">
            <div class="space-20"></div>

        </div>
    </div>
    <div class="space-20"></div>
    </div>
    </footer>
    <!--Footer-area-->
{{HTML::script('assets/js/constant.js')}}
<!--Vendor JS-->
{{--{{HTML::script('assets/js/vendor/jquery-1.12.4.min.js')}}--}}
{{HTML::script('assets/js/vendor/bootstrap.min.js')}}
<!--Plugin JS-->
{{HTML::script('assets/js/owl.carousel.min.js')}}
{{HTML::script('assets/js/scrollUp.min.js')}}
{{HTML::script('assets/js/magnific-popup.min.js')}}
{{HTML::script('assets/js/ripples-min.js')}}
{{HTML::script('assets/js/contact-form.js')}}
{{HTML::script('assets/js/spectragram.min.js')}}
{{HTML::script('assets/js/ajaxchimp.js')}}
{{HTML::script('assets/js/wow.min.js')}}
{{HTML::script('assets/js/plugins.js')}}
<!--Active JS-->
{{HTML::script('assets/js/main.js')}}
<!--Maps JS-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
{{HTML::script('assets/js/public.functions.js?v=0.01')}}
{{HTML::script('assets/js/maps.js')}}

{{HTML::script('assets/js/sign-board.js?v=0.01')}}
</body>
</html>
