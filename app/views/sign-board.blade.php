<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 7/01/18 20:51.
 */
?>
<?php   $variable = new VariableController();
$orgId = $variable->getVariable('orgid');
$server = $variable->getVariable('server');
?>

        <!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Your Bill Board</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{URL::To('/')}}/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="{{URL::To('/')}}/assets/images/icon.png" />

    <!-- Plugin-CSS -->
{{HTML::style('/assets/css/themify-icons.css')}}
{{HTML::style('/assets/css/bootstrap.min.css')}}
{{HTML::style('/assets/css/owl.carousel.min.css')}}

{{HTML::style('/assets/css/animate.css')}}
{{HTML::style('/assets/css/magnific-popup.css')}}

<!-- Main-Stylesheets -->
    {{HTML::style('/assets/css/space.css')}}
    {{HTML::style('/assets/css/theme.css')}}
    {{HTML::style('/assets/css/overright.css')}}
    {{HTML::style('/assets/css/normalize.css')}}
    {{HTML::style('/assets/css/style.css?v=0.01')}}
    {{HTML::style('/assets/css/responsive.css')}}
    {{HTML::script('assets/js/vendor/modernizr-2.8.3.min.js')}}

    {{--Image Uploader--}}
    <script src="https://crm.prisu.nz/assets/backend/libs/jquery/jquery-1.11.1.min.js"></script>
    <script src="https://crm.prisu.nz/assets/backend/libs/jquery-forms/jquery.form.js"></script>
</head>
<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="preloade">
    <span><i class="ti-blackboard"></i></span>
</div>

<!--Header-Area-->
<header class="relative fix" id="home">
    <!--Mainmenu-->
    <nav class="navbar navbar-default mainmenu-area navbar-fixed-top affix" data-offset-top="60">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index5.html#" class="navbar-brand">
                <!--<img src="{{URL::To('/')}}/assets/images/logo.png" alt="">-->
                    <img src="{{URL::To('/')}}/assets/images/logo.png" style="width: 250px; height: auto; margin-top: -10px;" />
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                <ul class="nav navbar-nav">
                    <li><a href="{{URL::To('/')}}#home">Home</a></li>
                    <li><a href="{{URL::To('/')}}#bill-boards">Bill Boards</a></li>
                    <li><a href="{{URL::To('/')}}#how-it-works">How It Works</a></li>
                    <li><a href="{{URL::To('/')}}#features">Special Features</a> </li>
                    <li><a href="{{URL::To('/')}}#faq">FAQ</a> </li>
                    <li><a href="{{URL::To('/')}}#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Mainmenu/-->
    <div class="space-100"></div>
    <div class="space-20 hidden-xs"></div>

    {{--Content start--}}



<!--Blog-Section-->
    <section style="max-height: 385px; overflow-y: auto;">
        <div class="space-80"></div>
        <div class="container">
            <div class="row">
                <h3 class="single-sign-title">{{ $billBoard->location }}</h3>
                <div class="col-xs-12 col-md-5">
                    <div class="blog-details">
                        <img src="https://crm.prisu.nz/assets/backend/images/signboard/boards/{{ $billBoard->img == 1 ? $billBoard->id : 'default' }}.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="space-20"></div>
                    <p style="text-align: justify;">{{ $billBoard->description }}</p>
                    <div class="space-40"></div>
                </div>
                @if(sizeof($billBoard->otherboards) > 0)
                <div class="col-xs-12 col-md-3">
                    <!--Sidebar-->
                    <aside class="sidebar" style="height: 340px !important; overflow-y: auto;">
                        <!--Sidebar-popular-post-->
                        <div class="single-sidebar">
                            <h3>Other Bill Boards</h3>
                            <hr>
                            <ul class="list-unstyled">
                                @foreach($billBoard->otherboards as $otherboard)
                                <li>
                                    <a href="{{URL::To('/')}}/sign-board/{{ $otherboard->id }}">
                                        <span class="alignleft">
                                           <img src="https://crm.prisu.nz/assets/backend/images/signboard/boards/{{ $otherboard->img == 1 ? $otherboard->id : 'default' }}.jpg" style="width: 100px;" alt="">
                                       </span>
                                        <h5>{{ $otherboard->location }}</h5>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </aside>
                    <!--Sidebar/-->
                </div>
                @endif
            </div>
        </div>
    </section>
    <!--Blog-Section/-->
    <hr>
    <!--Blog-reletad-Section-->
    <input type="hidden" id="boardid" name="boardid" value="{{ $billBoard->id }}" />
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <label>Select day/days to publish your sign</label>
                    <table class="calendar-table">
                        <thead>
                        <tr class="headings">
                            <th width="100">Sun</th>
                            <th width="100">Mon</th>
                            <th width="100">Tue</th>
                            <th width="100">Wed</th>
                            <th width="100">Thu</th>
                            <th width="100">Fri</th>
                            <th width="100">Sat</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $c = 0; ?>
                        @for($i = 1; $i < 6; $i++)
                            <tr>
                                @for($j = 1; $j < 8; $j++)

                                    <?php
                                        $day = $billBoard-> calendar[$c]->day;
                                        if($day == 'Sunday') $day = 1;
                                        else if($day == 'Monday') $day = 2;
                                        else if($day == 'Tuesday') $day = 3;
                                        else if($day == 'Wednesday') $day = 4;
                                        else if($day == 'Thursday') $day = 5;
                                        else if($day == 'Friday') $day = 6;
                                        else if($day == 'Saturday') $day = 7;

                                        $full = 0; //check booking slot is full or not

                                        if($billBoard->max_perday <= $billBoard-> calendar[$c]->booking_count) {
                                            $full = 1;
                                        }

                                        $id = $billBoard->id.$billBoard-> calendar[$c]->year.$billBoard-> calendar[$c]->month.$billBoard-> calendar[$c]->date;
                                    ?>

                                    <td @if($j == $day) class="calendar-active-cells
                                            @if($billBoard-> calendar[$c]->state == 0 && $billBoard-> calendar[$c]->singlerate)
                                                cell-disabled
                                            @elseif($full == 1)
                                                cell-disabled
                                            @endif"
                                            id="board-calendar-{{ $id }}" onmousedown="calendarPickDate({{$billBoard->id}}, {{ $billBoard-> calendar[$c]->year }}, {{ $billBoard-> calendar[$c]->month }}, {{ $billBoard-> calendar[$c]->date }});"
                                        @endif>
                                        @if($j == $day)
                                            <h5>{{ $billBoard->calendar[$c]->date }}<span> {{ $billBoard->calendar[$c]->short_month }}</span></h5><p id="board-cell-rate-{{ $id }}"
                                                @if($billBoard-> calendar[$c]->singlerate == null)
                                                    class="default-rate-value-{{ $billBoard->id }}"
                                                @endif>
                                                @if($billBoard-> calendar[$c]->singlerate != null && $billBoard-> calendar[$c]->state == 0)
                                                    <un>Unavailable</un>
                                                @elseif($full == 1)
                                                    <un style="font-size: 11px;">Full</un>
                                                @elseif($billBoard-> calendar[$c]->singlerate != null)
                                                    ${{ $billBoard-> calendar[$c]->singlerate }}
                                                @else
                                                    ${{ $billBoard-> defaultrate }}
                                                @endif</p>
                                            <?php $c++ ?>
                                        @else
                                            <h5>*</h5>
                                        @endif
                                    </td>
                                @endfor
                            </tr>
                        @endfor
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12 booking-form">
                        <div class="space-10"></div>
                        <div class="col-xs-12 col-sm-6 col-md-4 name">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" placeholder="Name">
                            <div class="space-15"></div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-8 email">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" placeholder="Email">
                            <div class="space-15"></div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-8 telephone">
                            <label for="email">Telephone:</label>
                            <input type="email" class="form-control" id="telephone" placeholder="Telephone">
                            <div class="space-15"></div>
                        </div>
                        <input type="hidden" id="img-id" value="" />
                        <div class="col-md-12 image">
                            {{--Upload image start--}}
                            <div class="upload-img-container" style="position: relative;" onclick="$('#image-choose-{{ $billBoard->id }}').trigger('click');">
                                <div class="upload-img-overlay">
                                    <h4>Upload your JPG/PNG file</h4><br/>
                                    <p>Click Here to Upload!<br/><br/><span  style="font-size: 14px;">Image needs to be JPG or PNG file format and dimensions need to be equal or greater than <strong>{{ $billBoard->width }} wide by {{ $billBoard->height }} high pixels</strong></span></p>
                                </div>
                                <label for="bill-board-upload-img">Upload your JPG/PNG file: <span id="image-error-display" style="color: #ff0b0b; font-weight: 400;"></span></label>
                                <img id="bill-board-upload-img" src="https://crm.prisu.nz/assets/backend/images/signboard/bookings/default.jpg" style="width: 100%; box-shadow: 1px 1px 2px 3px #eceaea;"/>
                                <div id = "image-upload-progress" class = "progress" style = "height: 8px;width: 100%;">
                                    <div id = "image-upload-progress-bar" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                        <span id="image-upload-sr-only" class = "sr-only">0% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <form action="https://crm.prisu.nz/signboard/uploadbookingimg?callback=?" method="post" id="upload-image-form-{{ $billBoard->id }}" enctype="multipart/form-data" class="bill-board-image-upload">
                                <input type="file" name="file" id="image-choose-{{ $billBoard->id }}" onchange="submitImageUploadForm('#upload-image-form-{{ $billBoard->id }}');" accept="image/jpg;capture=camera" style="display: none;">
                                <input type="hidden" id="orgid" name="orgid" value="{{ $orgId }}" />
                            </form>
                            {{--Upload image end--}}
                        </div>
                        <div class="col-md-12">
                            <table class="price-table">
                                <thead>
                                <tr class="headings">
                                    <th>Day</th>
                                    <th align="right" style="text-align: right;">Price</th>
                                </tr>
                                </thead>
                                <tbody id="booked-rows">
                                    {{--JQuery append--}}
                                </tbody>
                                <tfoot id="table-footer">
                                <tr class="gst-row">
                                    <td style="padding-top: 15px;">GST</td>
                                    <td style="padding-top: 15px;" id="booking-gst" align="right"></td>
                                </tr>
                                <tr class="total-row">
                                    <td>Total</td>
                                    <td id="booking-total" align="right"></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div id="polipay-button-container" class="col-md-12">
                            <img class="pull-right" style="cursor: pointer; margin-top: 20px;" onclick="payNow();" src="{{ $server }}/assets/backend/images/merchants/pay-buttons/poli.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    {{--content end--}}

    <div class="space-80"></div>
    <div class="row text-white wow fadeIn ifooter">
        <div class="col-xs-12 text-center">
            <div class="space-20"></div>
            <p>{{ date('Y') }} All rights reserved.</p>
        </div>
    </div>
    <div class="space-20"></div>
    </div>
    </footer>
    <!--Footer-area-->
{{HTML::script('assets/js/constant.js?v=0.01')}}
<!--Vendor JS-->
{{--{{HTML::script('assets/js/vendor/jquery-1.12.4.min.js')}}--}}
{{HTML::script('assets/js/vendor/bootstrap.min.js')}}
<!--Plugin JS-->
{{HTML::script('assets/js/owl.carousel.min.js')}}
{{HTML::script('assets/js/scrollUp.min.js')}}
{{HTML::script('assets/js/magnific-popup.min.js')}}
{{HTML::script('assets/js/ripples-min.js')}}
{{HTML::script('assets/js/contact-form.js')}}
{{HTML::script('assets/js/spectragram.min.js')}}
{{HTML::script('assets/js/ajaxchimp.js')}}
{{HTML::script('assets/js/wow.min.js')}}
{{HTML::script('assets/js/plugins.js')}}
<!--Active JS-->
{{HTML::script('assets/js/main.js')}}
<!--Maps JS-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
{{HTML::script('assets/js/public.functions.js?v=0.01')}}
{{HTML::script('assets/js/maps.js')}}

{{HTML::script('assets/js/sign-board.js?v=0.18')}}
</body>
</html>