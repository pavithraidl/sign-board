<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@getHome'
));

Route::get('/sign-board/{boardId}', array(
    'as' => 'single-board',
    'uses' => 'HomeController@getSingleBoard'
));

Route::get('/payments/{type}/{bookingId}', array(
    'as' => 'payments',
    'uses' => 'HomeController@getPayments'
));

Route::get('/admin', function () {
    return Redirect::to('https://crm.prisu.nz/');
});


Route::get('/test', function() {

});