<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getHome()
	{
	    $contentObject = $this->getWebsiteData();

//	    dd($contentObject);
		return View::make('default', array(
		    'contentObject' => $contentObject,
            'orgId' => VariableController::orgId
        ));
	}

	public function getSingleBoard($boardId) {
        $requestUrl = sprintf('https://crm.prisu.nz/signboard/getsignboardwebsitedata/%s/%s', 7, $boardId);
        $response = file_get_contents($requestUrl);
        $response = json_decode($response);

        if(is_object($response)) {
            return View::make('sign-board', array(
                'billBoard' => $response
            ));

        }
        else {
            return Redirect::to(URL::To('/'));
        }
    }

    public function getPayments($type, $bookingId) {

	    if($type == 'success')
	        $status = 1;
	    else if($type == 'fail' || $type == 'cancel')
	        $status = -1;


        $url = 'https://crm.prisu.nz/signboard/purchasestatus';
        $ch = curl_init($url);
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        //The JSON data.

        $jsonData = array(
            'orgid' => 7,
            'status' => $status,
            'userip' => $ip,
            'bookingid' => $bookingId
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return View::make('payment-success', array(
            'id' => $bookingId,
            'status' => $status,
            'booking' => $result
        ));
    }

    function getWebsiteData() {
        $sliderIds = [39, 40, 41, 42, 43, 44, 45];
        //API Url
        $url = 'https://crm.prisu.nz/website/getsliders';
        $ch = curl_init($url);

        //The JSON data.

        $jsonData = array(
            'orgid' => VariableController::orgId,
            'orgkey' => VariableController::key,
            'orgsecret' => VariableController::secret,
            'sliderids' => $sliderIds
        );

        //Encode the array into JSON.
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        //Execute the request
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        return $result;
    }

}
